﻿namespace ReleaseWaveletAlogorithm
{
    partial class FormSetDirections
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_Main = new System.Windows.Forms.Panel();
            this.panel_MainCenter = new System.Windows.Forms.Panel();
            this.comboBox_TopLeft = new System.Windows.Forms.ComboBox();
            this.comboBox_Left = new System.Windows.Forms.ComboBox();
            this.comboBox_BottonLeft = new System.Windows.Forms.ComboBox();
            this.comboBox_Botton = new System.Windows.Forms.ComboBox();
            this.comboBox_BottonRight = new System.Windows.Forms.ComboBox();
            this.comboBox_Right = new System.Windows.Forms.ComboBox();
            this.comboBox_TopRight = new System.Windows.Forms.ComboBox();
            this.comboBox_Top = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel_Main_Botton = new System.Windows.Forms.Panel();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_Save = new System.Windows.Forms.Button();
            this.panel_MainTop = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_Main.SuspendLayout();
            this.panel_MainCenter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel_Main_Botton.SuspendLayout();
            this.panel_MainTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Main
            // 
            this.panel_Main.BackColor = System.Drawing.Color.Silver;
            this.panel_Main.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Main.Controls.Add(this.panel_MainCenter);
            this.panel_Main.Controls.Add(this.panel_Main_Botton);
            this.panel_Main.Controls.Add(this.panel_MainTop);
            this.panel_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Main.Location = new System.Drawing.Point(0, 0);
            this.panel_Main.Name = "panel_Main";
            this.panel_Main.Size = new System.Drawing.Size(420, 500);
            this.panel_Main.TabIndex = 0;
            // 
            // panel_MainCenter
            // 
            this.panel_MainCenter.Controls.Add(this.comboBox_TopLeft);
            this.panel_MainCenter.Controls.Add(this.comboBox_Left);
            this.panel_MainCenter.Controls.Add(this.comboBox_BottonLeft);
            this.panel_MainCenter.Controls.Add(this.comboBox_Botton);
            this.panel_MainCenter.Controls.Add(this.comboBox_BottonRight);
            this.panel_MainCenter.Controls.Add(this.comboBox_Right);
            this.panel_MainCenter.Controls.Add(this.comboBox_TopRight);
            this.panel_MainCenter.Controls.Add(this.comboBox_Top);
            this.panel_MainCenter.Controls.Add(this.pictureBox1);
            this.panel_MainCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_MainCenter.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.panel_MainCenter.Location = new System.Drawing.Point(0, 33);
            this.panel_MainCenter.Name = "panel_MainCenter";
            this.panel_MainCenter.Size = new System.Drawing.Size(418, 410);
            this.panel_MainCenter.TabIndex = 2;
            // 
            // comboBox_TopLeft
            // 
            this.comboBox_TopLeft.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_TopLeft.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_TopLeft.FormattingEnabled = true;
            this.comboBox_TopLeft.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.comboBox_TopLeft.Location = new System.Drawing.Point(114, 124);
            this.comboBox_TopLeft.Name = "comboBox_TopLeft";
            this.comboBox_TopLeft.Size = new System.Drawing.Size(50, 34);
            this.comboBox_TopLeft.TabIndex = 1;
            // 
            // comboBox_Left
            // 
            this.comboBox_Left.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Left.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_Left.FormattingEnabled = true;
            this.comboBox_Left.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.comboBox_Left.Location = new System.Drawing.Point(85, 188);
            this.comboBox_Left.Name = "comboBox_Left";
            this.comboBox_Left.Size = new System.Drawing.Size(50, 34);
            this.comboBox_Left.TabIndex = 1;
            // 
            // comboBox_BottonLeft
            // 
            this.comboBox_BottonLeft.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_BottonLeft.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_BottonLeft.FormattingEnabled = true;
            this.comboBox_BottonLeft.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.comboBox_BottonLeft.Location = new System.Drawing.Point(116, 254);
            this.comboBox_BottonLeft.Name = "comboBox_BottonLeft";
            this.comboBox_BottonLeft.Size = new System.Drawing.Size(50, 34);
            this.comboBox_BottonLeft.TabIndex = 1;
            // 
            // comboBox_Botton
            // 
            this.comboBox_Botton.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Botton.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_Botton.FormattingEnabled = true;
            this.comboBox_Botton.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.comboBox_Botton.Location = new System.Drawing.Point(185, 295);
            this.comboBox_Botton.Name = "comboBox_Botton";
            this.comboBox_Botton.Size = new System.Drawing.Size(50, 34);
            this.comboBox_Botton.TabIndex = 1;
            // 
            // comboBox_BottonRight
            // 
            this.comboBox_BottonRight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_BottonRight.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_BottonRight.FormattingEnabled = true;
            this.comboBox_BottonRight.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.comboBox_BottonRight.Location = new System.Drawing.Point(253, 254);
            this.comboBox_BottonRight.Name = "comboBox_BottonRight";
            this.comboBox_BottonRight.Size = new System.Drawing.Size(50, 34);
            this.comboBox_BottonRight.TabIndex = 1;
            // 
            // comboBox_Right
            // 
            this.comboBox_Right.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Right.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_Right.FormattingEnabled = true;
            this.comboBox_Right.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.comboBox_Right.Location = new System.Drawing.Point(283, 188);
            this.comboBox_Right.Name = "comboBox_Right";
            this.comboBox_Right.Size = new System.Drawing.Size(50, 34);
            this.comboBox_Right.TabIndex = 1;
            // 
            // comboBox_TopRight
            // 
            this.comboBox_TopRight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_TopRight.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_TopRight.FormattingEnabled = true;
            this.comboBox_TopRight.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.comboBox_TopRight.Location = new System.Drawing.Point(253, 124);
            this.comboBox_TopRight.Name = "comboBox_TopRight";
            this.comboBox_TopRight.Size = new System.Drawing.Size(50, 34);
            this.comboBox_TopRight.TabIndex = 1;
            // 
            // comboBox_Top
            // 
            this.comboBox_Top.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Top.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.comboBox_Top.FormattingEnabled = true;
            this.comboBox_Top.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.comboBox_Top.Location = new System.Drawing.Point(185, 81);
            this.comboBox_Top.Name = "comboBox_Top";
            this.comboBox_Top.Size = new System.Drawing.Size(50, 34);
            this.comboBox_Top.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::ReleaseWaveletAlogorithm.Properties.Resources.D;
            this.pictureBox1.Location = new System.Drawing.Point(10, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(398, 390);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel_Main_Botton
            // 
            this.panel_Main_Botton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.panel_Main_Botton.Controls.Add(this.button_Cancel);
            this.panel_Main_Botton.Controls.Add(this.button_Save);
            this.panel_Main_Botton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_Main_Botton.Font = new System.Drawing.Font("Lay\'s", 15.75F, System.Drawing.FontStyle.Bold);
            this.panel_Main_Botton.Location = new System.Drawing.Point(0, 443);
            this.panel_Main_Botton.Name = "panel_Main_Botton";
            this.panel_Main_Botton.Size = new System.Drawing.Size(418, 55);
            this.panel_Main_Botton.TabIndex = 1;
            // 
            // button_Cancel
            // 
            this.button_Cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Cancel.Dock = System.Windows.Forms.DockStyle.Left;
            this.button_Cancel.FlatAppearance.BorderSize = 0;
            this.button_Cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Cancel.ForeColor = System.Drawing.Color.White;
            this.button_Cancel.Location = new System.Drawing.Point(0, 0);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(175, 55);
            this.button_Cancel.TabIndex = 0;
            this.button_Cancel.Text = "Отмена";
            this.button_Cancel.UseVisualStyleBackColor = true;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_Save
            // 
            this.button_Save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Save.Dock = System.Windows.Forms.DockStyle.Right;
            this.button_Save.FlatAppearance.BorderSize = 0;
            this.button_Save.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Save.ForeColor = System.Drawing.Color.White;
            this.button_Save.Location = new System.Drawing.Point(243, 0);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(175, 55);
            this.button_Save.TabIndex = 0;
            this.button_Save.Text = "Сохранить";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // panel_MainTop
            // 
            this.panel_MainTop.BackColor = System.Drawing.Color.White;
            this.panel_MainTop.Controls.Add(this.label1);
            this.panel_MainTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_MainTop.Font = new System.Drawing.Font("Open Sans", 15.75F, System.Drawing.FontStyle.Bold);
            this.panel_MainTop.Location = new System.Drawing.Point(0, 0);
            this.panel_MainTop.Name = "panel_MainTop";
            this.panel_MainTop.Size = new System.Drawing.Size(418, 33);
            this.panel_MainTop.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(229, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Задать напраления";
            // 
            // FormSetDirections
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(420, 500);
            this.Controls.Add(this.panel_Main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormSetDirections";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormSetDirections";
            this.panel_Main.ResumeLayout(false);
            this.panel_MainCenter.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel_Main_Botton.ResumeLayout(false);
            this.panel_MainTop.ResumeLayout(false);
            this.panel_MainTop.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_Main;
        private System.Windows.Forms.Panel panel_Main_Botton;
        private System.Windows.Forms.Panel panel_MainTop;
        private System.Windows.Forms.Panel panel_MainCenter;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBox_TopLeft;
        private System.Windows.Forms.ComboBox comboBox_Left;
        private System.Windows.Forms.ComboBox comboBox_BottonLeft;
        private System.Windows.Forms.ComboBox comboBox_Botton;
        private System.Windows.Forms.ComboBox comboBox_BottonRight;
        private System.Windows.Forms.ComboBox comboBox_Right;
        private System.Windows.Forms.ComboBox comboBox_TopRight;
        private System.Windows.Forms.ComboBox comboBox_Top;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Label label1;
    }
}