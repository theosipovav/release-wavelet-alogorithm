﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReleaseWaveletAlogorithm
{
    public partial class FormHelp : Form
    {
        public FormHelp(int _inNH)
        {
            InitializeComponent();
            switch (_inNH)
            {
                case 1:
                    pictureBox_H.Image = ReleaseWaveletAlogorithm.Properties.Resources.H1;
                    break;
                case 2:
                    pictureBox_H.Image = ReleaseWaveletAlogorithm.Properties.Resources.H2;
                    break;
                default:
                    break;

            }
        }
    }
}
