﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReleaseWaveletAlogorithm
{
    public partial class FormSetDirections : Form
    {
        FormMain _FM;
        int _Top, _TopRight, _Right, _BottonRight, _Botton, _BottonLeft, _Left, _TopLeft;
        public FormSetDirections(FormMain _InFM)
        {
            InitializeComponent();
            _FM = _InFM;
            comboBox_Top.Text = _FM._ClassDkp._Directions[0].ToString();
            comboBox_TopRight.Text = _FM._ClassDkp._Directions[1].ToString();
            comboBox_Right.Text = _FM._ClassDkp._Directions[2].ToString();
            comboBox_BottonRight.Text = _FM._ClassDkp._Directions[3].ToString();
            comboBox_Botton.Text = _FM._ClassDkp._Directions[4].ToString();
            comboBox_BottonLeft.Text = _FM._ClassDkp._Directions[5].ToString();
            comboBox_Left.Text = _FM._ClassDkp._Directions[6].ToString();
            comboBox_TopLeft.Text = _FM._ClassDkp._Directions[7].ToString();
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            _Top = Convert.ToInt32(comboBox_Top.Text);
            _TopRight = Convert.ToInt32(comboBox_TopRight.Text);
            _Right = Convert.ToInt32(comboBox_Right.Text);
            _BottonRight = Convert.ToInt32(comboBox_BottonRight.Text);
            _Botton = Convert.ToInt32(comboBox_Botton.Text);
            _BottonLeft = Convert.ToInt32(comboBox_BottonLeft.Text);
            _Left = Convert.ToInt32(comboBox_Left.Text);
            _TopLeft = Convert.ToInt32(comboBox_TopLeft.Text);

            if ((_Top == _TopRight) || (_Top == _Right) || (_Top == _BottonRight) || (_Top == _Botton) || (_Top == _BottonLeft) || (_Top == _Left) || (_Top == _TopLeft))
            {
                MessageBox.Show("Значения должны быть разными.", "Ошибка!");
            }
            else
            {
                if ((_TopRight == _Right) || (_TopRight == _BottonRight) || (_TopRight == _Botton) || (_TopRight == _BottonLeft) || (_TopRight == _Left) || (_TopRight == _TopLeft))
                {
                    MessageBox.Show("Значения должны быть разными.","Ошибка!");
                }
                else
                {
                    if ((_Right == _BottonRight) || (_Right == _Botton) || (_Right == _BottonLeft) || (_Right == _Left) || (_Right == _TopLeft))
                    {
                        MessageBox.Show("Значения должны быть разными.", "Ошибка!");
                    }
                    else
                    {
                        if ((_BottonRight == _Botton) || (_BottonRight == _BottonLeft) || (_BottonRight == _Left) || (_BottonRight == _TopLeft))
                        {
                            MessageBox.Show("Значения должны быть разными.", "Ошибка!");
                        }
                        else
                        {
                            if ((_Botton == _BottonLeft) || (_Botton == _Left) || (_Botton == _TopLeft))
                            {
                                MessageBox.Show("Значения должны быть разными.", "Ошибка!");
                            }
                            else
                            {
                                if ((_BottonLeft == _Left) || (_BottonLeft == _TopLeft))
                                {
                                    MessageBox.Show("Значения должны быть разными.", "Ошибка!");
                                }
                                else
                                {
                                    if (_Left == _TopLeft)
                                    {
                                        MessageBox.Show("Значения должны быть разными.", "Ошибка!");
                                    }
                                    else
                                    {
                                        _FM._ClassDkp.SetDirections(_Top, _TopRight, _Right, _BottonRight, _Botton, _BottonLeft, _Left, _TopLeft);
                                        this.Close();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
