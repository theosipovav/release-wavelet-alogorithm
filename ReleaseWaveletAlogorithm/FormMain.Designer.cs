﻿namespace ReleaseWaveletAlogorithm
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_MainTop = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox_Tools = new System.Windows.Forms.GroupBox();
            this.checkBox_DelayUnit = new System.Windows.Forms.CheckBox();
            this.button_Tracing = new System.Windows.Forms.Button();
            this.button_WaveCreate = new System.Windows.Forms.Button();
            this.button_SetX = new System.Windows.Forms.Button();
            this.button_SetOut = new System.Windows.Forms.Button();
            this.button_SetIn = new System.Windows.Forms.Button();
            this.button_Directions = new System.Windows.Forms.Button();
            this.button_Exit = new System.Windows.Forms.Button();
            this.button_ProjectSave = new System.Windows.Forms.Button();
            this.button_ProjectCreate = new System.Windows.Forms.Button();
            this.button_ProjectOpen = new System.Windows.Forms.Button();
            this.panel_Main = new System.Windows.Forms.Panel();
            this.panel_DrawDkp = new System.Windows.Forms.Panel();
            this.button_Help = new System.Windows.Forms.Button();
            this.panel_MainTop.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox_Tools.SuspendLayout();
            this.panel_Main.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_MainTop
            // 
            this.panel_MainTop.BackColor = System.Drawing.Color.White;
            this.panel_MainTop.Controls.Add(this.label1);
            this.panel_MainTop.Controls.Add(this.panel3);
            this.panel_MainTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_MainTop.Location = new System.Drawing.Point(0, 0);
            this.panel_MainTop.Name = "panel_MainTop";
            this.panel_MainTop.Size = new System.Drawing.Size(798, 33);
            this.panel_MainTop.TabIndex = 0;
            this.panel_MainTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_MainTop_MouseDown);
            this.panel_MainTop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_MainTop_MouseMove);
            this.panel_MainTop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_MainTop_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Open Sans", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.label1.Location = new System.Drawing.Point(182, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(234, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Волновой алгоритм";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(179, 33);
            this.panel3.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.panel2.Controls.Add(this.groupBox_Tools);
            this.panel2.Controls.Add(this.button_Help);
            this.panel2.Controls.Add(this.button_Exit);
            this.panel2.Controls.Add(this.button_ProjectSave);
            this.panel2.Controls.Add(this.button_ProjectCreate);
            this.panel2.Controls.Add(this.button_ProjectOpen);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Font = new System.Drawing.Font("Lay\'s", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(0, 33);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(179, 565);
            this.panel2.TabIndex = 1;
            // 
            // groupBox_Tools
            // 
            this.groupBox_Tools.Controls.Add(this.checkBox_DelayUnit);
            this.groupBox_Tools.Controls.Add(this.button_Tracing);
            this.groupBox_Tools.Controls.Add(this.button_WaveCreate);
            this.groupBox_Tools.Controls.Add(this.button_SetX);
            this.groupBox_Tools.Controls.Add(this.button_SetOut);
            this.groupBox_Tools.Controls.Add(this.button_SetIn);
            this.groupBox_Tools.Controls.Add(this.button_Directions);
            this.groupBox_Tools.Enabled = false;
            this.groupBox_Tools.Font = new System.Drawing.Font("Open Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox_Tools.ForeColor = System.Drawing.Color.White;
            this.groupBox_Tools.Location = new System.Drawing.Point(3, 153);
            this.groupBox_Tools.Name = "groupBox_Tools";
            this.groupBox_Tools.Size = new System.Drawing.Size(173, 353);
            this.groupBox_Tools.TabIndex = 1;
            this.groupBox_Tools.TabStop = false;
            this.groupBox_Tools.Text = "Инструменты:";
            // 
            // checkBox_DelayUnit
            // 
            this.checkBox_DelayUnit.AutoSize = true;
            this.checkBox_DelayUnit.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBox_DelayUnit.Location = new System.Drawing.Point(14, 325);
            this.checkBox_DelayUnit.Name = "checkBox_DelayUnit";
            this.checkBox_DelayUnit.Size = new System.Drawing.Size(142, 22);
            this.checkBox_DelayUnit.TabIndex = 1;
            this.checkBox_DelayUnit.Text = "Аппарат задержки";
            this.checkBox_DelayUnit.UseVisualStyleBackColor = true;
            // 
            // button_Tracing
            // 
            this.button_Tracing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.button_Tracing.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Tracing.Enabled = false;
            this.button_Tracing.FlatAppearance.BorderSize = 0;
            this.button_Tracing.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_Tracing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Tracing.Font = new System.Drawing.Font("Lay\'s", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_Tracing.Location = new System.Drawing.Point(5, 273);
            this.button_Tracing.Name = "button_Tracing";
            this.button_Tracing.Size = new System.Drawing.Size(163, 50);
            this.button_Tracing.TabIndex = 0;
            this.button_Tracing.Text = "Выполнить трассироку";
            this.button_Tracing.UseVisualStyleBackColor = false;
            this.button_Tracing.Click += new System.EventHandler(this.button_Tracing_Click);
            // 
            // button_WaveCreate
            // 
            this.button_WaveCreate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.button_WaveCreate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_WaveCreate.Enabled = false;
            this.button_WaveCreate.FlatAppearance.BorderSize = 0;
            this.button_WaveCreate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_WaveCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_WaveCreate.Font = new System.Drawing.Font("Lay\'s", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_WaveCreate.Location = new System.Drawing.Point(5, 173);
            this.button_WaveCreate.Name = "button_WaveCreate";
            this.button_WaveCreate.Size = new System.Drawing.Size(163, 50);
            this.button_WaveCreate.TabIndex = 0;
            this.button_WaveCreate.Text = "Распределение волны";
            this.button_WaveCreate.UseVisualStyleBackColor = false;
            this.button_WaveCreate.Click += new System.EventHandler(this.button_WaveCreate_Click);
            // 
            // button_SetX
            // 
            this.button_SetX.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.button_SetX.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_SetX.FlatAppearance.BorderSize = 0;
            this.button_SetX.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_SetX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_SetX.Font = new System.Drawing.Font("Lay\'s", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_SetX.Location = new System.Drawing.Point(5, 123);
            this.button_SetX.Name = "button_SetX";
            this.button_SetX.Size = new System.Drawing.Size(163, 50);
            this.button_SetX.TabIndex = 0;
            this.button_SetX.Text = "Задать/убрать препятствие";
            this.button_SetX.UseVisualStyleBackColor = false;
            this.button_SetX.Click += new System.EventHandler(this.button_SetX_Click);
            // 
            // button_SetOut
            // 
            this.button_SetOut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.button_SetOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_SetOut.FlatAppearance.BorderSize = 0;
            this.button_SetOut.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_SetOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_SetOut.Font = new System.Drawing.Font("Lay\'s", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_SetOut.Location = new System.Drawing.Point(5, 73);
            this.button_SetOut.Name = "button_SetOut";
            this.button_SetOut.Size = new System.Drawing.Size(163, 50);
            this.button_SetOut.TabIndex = 0;
            this.button_SetOut.Text = "Задать приемник";
            this.button_SetOut.UseVisualStyleBackColor = false;
            this.button_SetOut.Click += new System.EventHandler(this.button_SetOut_Click);
            // 
            // button_SetIn
            // 
            this.button_SetIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.button_SetIn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_SetIn.FlatAppearance.BorderSize = 0;
            this.button_SetIn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_SetIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_SetIn.Font = new System.Drawing.Font("Lay\'s", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_SetIn.Location = new System.Drawing.Point(5, 23);
            this.button_SetIn.Name = "button_SetIn";
            this.button_SetIn.Size = new System.Drawing.Size(163, 50);
            this.button_SetIn.TabIndex = 0;
            this.button_SetIn.Text = "Задать источник";
            this.button_SetIn.UseVisualStyleBackColor = false;
            this.button_SetIn.Click += new System.EventHandler(this.button_SetIn_Click);
            // 
            // button_Directions
            // 
            this.button_Directions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.button_Directions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Directions.FlatAppearance.BorderSize = 0;
            this.button_Directions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_Directions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Directions.Font = new System.Drawing.Font("Lay\'s", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_Directions.Location = new System.Drawing.Point(5, 223);
            this.button_Directions.Name = "button_Directions";
            this.button_Directions.Size = new System.Drawing.Size(163, 50);
            this.button_Directions.TabIndex = 0;
            this.button_Directions.Text = "Приоритеты направлений";
            this.button_Directions.UseVisualStyleBackColor = false;
            this.button_Directions.Click += new System.EventHandler(this.button_Directions_Click);
            // 
            // button_Exit
            // 
            this.button_Exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Exit.FlatAppearance.BorderSize = 0;
            this.button_Exit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Exit.Location = new System.Drawing.Point(3, 512);
            this.button_Exit.Name = "button_Exit";
            this.button_Exit.Size = new System.Drawing.Size(146, 50);
            this.button_Exit.TabIndex = 0;
            this.button_Exit.Text = "Выход";
            this.button_Exit.UseVisualStyleBackColor = true;
            this.button_Exit.Click += new System.EventHandler(this.button_Exit_Click);
            // 
            // button_ProjectSave
            // 
            this.button_ProjectSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_ProjectSave.Enabled = false;
            this.button_ProjectSave.FlatAppearance.BorderSize = 0;
            this.button_ProjectSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_ProjectSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ProjectSave.Location = new System.Drawing.Point(3, 103);
            this.button_ProjectSave.Name = "button_ProjectSave";
            this.button_ProjectSave.Size = new System.Drawing.Size(175, 50);
            this.button_ProjectSave.TabIndex = 0;
            this.button_ProjectSave.Text = "Сохранить";
            this.button_ProjectSave.UseVisualStyleBackColor = true;
            this.button_ProjectSave.Click += new System.EventHandler(this.button_ProjectSave_Click);
            // 
            // button_ProjectCreate
            // 
            this.button_ProjectCreate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_ProjectCreate.FlatAppearance.BorderSize = 0;
            this.button_ProjectCreate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_ProjectCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ProjectCreate.Location = new System.Drawing.Point(3, 3);
            this.button_ProjectCreate.Name = "button_ProjectCreate";
            this.button_ProjectCreate.Size = new System.Drawing.Size(175, 50);
            this.button_ProjectCreate.TabIndex = 0;
            this.button_ProjectCreate.Text = "Новый проект";
            this.button_ProjectCreate.UseVisualStyleBackColor = true;
            this.button_ProjectCreate.Click += new System.EventHandler(this.button_CreateProject_Click);
            // 
            // button_ProjectOpen
            // 
            this.button_ProjectOpen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_ProjectOpen.FlatAppearance.BorderSize = 0;
            this.button_ProjectOpen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_ProjectOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ProjectOpen.Location = new System.Drawing.Point(3, 53);
            this.button_ProjectOpen.Name = "button_ProjectOpen";
            this.button_ProjectOpen.Size = new System.Drawing.Size(175, 50);
            this.button_ProjectOpen.TabIndex = 0;
            this.button_ProjectOpen.Text = "Открыть";
            this.button_ProjectOpen.UseVisualStyleBackColor = true;
            this.button_ProjectOpen.Click += new System.EventHandler(this.button_ProjectOpen_Click);
            // 
            // panel_Main
            // 
            this.panel_Main.BackColor = System.Drawing.Color.Silver;
            this.panel_Main.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Main.Controls.Add(this.panel_DrawDkp);
            this.panel_Main.Controls.Add(this.panel2);
            this.panel_Main.Controls.Add(this.panel_MainTop);
            this.panel_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Main.Location = new System.Drawing.Point(0, 0);
            this.panel_Main.Name = "panel_Main";
            this.panel_Main.Size = new System.Drawing.Size(800, 600);
            this.panel_Main.TabIndex = 2;
            // 
            // panel_DrawDkp
            // 
            this.panel_DrawDkp.BackColor = System.Drawing.Color.Honeydew;
            this.panel_DrawDkp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_DrawDkp.Font = new System.Drawing.Font("Open Sans", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel_DrawDkp.Location = new System.Drawing.Point(187, 39);
            this.panel_DrawDkp.Name = "panel_DrawDkp";
            this.panel_DrawDkp.Size = new System.Drawing.Size(600, 550);
            this.panel_DrawDkp.TabIndex = 2;
            // 
            // button_Help
            // 
            this.button_Help.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Help.FlatAppearance.BorderSize = 0;
            this.button_Help.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_Help.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Help.Location = new System.Drawing.Point(155, 512);
            this.button_Help.Name = "button_Help";
            this.button_Help.Size = new System.Drawing.Size(21, 50);
            this.button_Help.TabIndex = 0;
            this.button_Help.Text = "?";
            this.button_Help.UseVisualStyleBackColor = true;
            this.button_Help.Click += new System.EventHandler(this.button_Help_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.panel_Main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Волновой алгоритм";
            this.panel_MainTop.ResumeLayout(false);
            this.panel_MainTop.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox_Tools.ResumeLayout(false);
            this.groupBox_Tools.PerformLayout();
            this.panel_Main.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_MainTop;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button_Exit;
        private System.Windows.Forms.Panel panel_Main;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_SetIn;
        private System.Windows.Forms.Button button_Tracing;
        private System.Windows.Forms.Button button_WaveCreate;
        private System.Windows.Forms.Button button_SetX;
        private System.Windows.Forms.Button button_SetOut;
        private System.Windows.Forms.Button button_Directions;
        private System.Windows.Forms.CheckBox checkBox_DelayUnit;
        public System.Windows.Forms.GroupBox groupBox_Tools;
        private System.Windows.Forms.Panel panel_DrawDkp;
        public System.Windows.Forms.Button button_ProjectOpen;
        public System.Windows.Forms.Button button_ProjectSave;
        public System.Windows.Forms.Button button_ProjectCreate;
        private System.Windows.Forms.Button button_Help;
    }
}

