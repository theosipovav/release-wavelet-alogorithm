﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReleaseWaveletAlogorithm
{
    public class ClassDkp
    {
        public string _NameProject;
        public int _Height, _Width, _CellSize;
        public Point _ElementA;
        public Point _ElementB;
        public List<Point> _ElementsX;
        public int[] _Directions;

        public ClassDkp()
        {
            _NameProject = null;
            _Height = -1;
            _Width = -1;
            _CellSize = -1;
            _ElementA = new Point(-1, -1);
            _ElementB = new Point(-1, -1);
            _ElementsX = new List<Point>();
            _Directions = new int[8] { 1, 2, 3, 4, 5, 6, 7, 8 };

        }

        public void SetDirections(int _InD1, int _InD2, int _InD3, int _InD4, int _InD5, int _InD6, int _InD7, int _InD8)
        {
            _Directions[0] = _InD1;
            _Directions[1] = _InD2;
            _Directions[2] = _InD3;
            _Directions[3] = _InD4;
            _Directions[4] = _InD5;
            _Directions[5] = _InD6;
            _Directions[6] = _InD7;
            _Directions[7] = _InD8;
        }

    }
}
