﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;

namespace ReleaseWaveletAlogorithm
{
    public partial class FormCreateProject : Form
    {
        FormMain _FM;
        private Int32 _TmpX;
        private Int32 _TmpY;
        private bool _FMove;
        public FormCreateProject(FormMain _InFM)
        {
            InitializeComponent();
            _FM = _InFM;
            textBox_NameProject.Text = "Проект_" + DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
        }

        // Перемещение формы
        private void panel_MainTop_MouseDown(object sender, MouseEventArgs e)
        {
            this.Opacity = 0.5;
            _TmpX = Cursor.Position.X;
            _TmpY = Cursor.Position.Y;
            _FMove = true;
        }
        private void panel_MainTop_MouseUp(object sender, MouseEventArgs e)
        {
            this.Opacity = 1.0;
            _FMove = false;
        }
        private void panel_MainTop_MouseMove(object sender, MouseEventArgs e)
        {
            if (_FMove)
            {
                this.Left = this.Left + (Cursor.Position.X - _TmpX);
                this.Top = this.Top + (Cursor.Position.Y - _TmpY);

                _TmpX = Cursor.Position.X;
                _TmpY = Cursor.Position.Y;
            }
        }
        private void button_Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Create_Click(object sender, EventArgs e)
        {
            if ((textBox_NameProject.Text.Length > 0) && (comboBox_DkpHeight.Text.Length > 0) &&
                (comboBox_DkpWidth.Text.Length > 0) && (textBox_DkpCell.Text.Length > 0))
            {
                try
                {
                    _FM._ClassDkp = new ClassDkp();
                    _FM._ClassDkp._NameProject = textBox_NameProject.Text;
                    _FM._ClassDkp._Height = Convert.ToInt32(comboBox_DkpHeight.Text);
                    _FM._ClassDkp._Width = Convert.ToInt32(comboBox_DkpWidth.Text);
                    _FM._ClassDkp._CellSize = Convert.ToInt32(textBox_DkpCell.Text);
                    _FM.groupBox_Tools.Enabled = true;
                    _FM.button_ProjectSave.Enabled = true;
                    _FM.button_ProjectCreate.ForeColor = Color.Cyan;
                    _FM.button_ProjectOpen.ForeColor = Color.Cyan;
                    _FM.DrawDKP();
                    this.Close();
                }
                catch (Exception _Exception)
                {
                    MessageBox.Show(_Exception.Message, "Ошибка!");
                    _FM._ClassDkp = new ClassDkp();
                    _FM.groupBox_Tools.Enabled = false;
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля.","Внимание!");
            }
        }

        private void button_Help_Click(object sender, EventArgs e)
        {
            FormHelp _FH = new FormHelp(2);
            _FH.ShowDialog();
        }
    }
}
