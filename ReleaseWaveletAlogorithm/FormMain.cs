﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Collections;

namespace ReleaseWaveletAlogorithm
{
    public partial class FormMain : Form
    {
        private Int32 _TmpX;
        private Int32 _TmpY;
        private bool _FMove;
        public ClassDkp _ClassDkp;
        List<List<TextBox>> _Dkp = new List<List<TextBox>>();
        public FormMain()
        {
            InitializeComponent();
            _FMove = false;

        }
        public void DrawDKP()
        {
            if ((_ClassDkp._NameProject != null) && (_ClassDkp._Height >= 5) && (_ClassDkp._Width >= 5) && (_ClassDkp._CellSize > 0))
            {
                // Инициализарус коллекцию TexBox для отображения поля
                for (int nY =0; nY < _ClassDkp._Height; nY++)
                {
                    List<TextBox> _LineDkp = new List<TextBox>();
                    for (int nX = 0; nX < _ClassDkp._Width; nX++)
                    {
                        _LineDkp.Add(new TextBox());
                    }
                    _Dkp.Add(_LineDkp);
                }
                // Отрисовка поля
                int _CellSizeDraw = 30;
                panel_DrawDkp.Height = (_CellSizeDraw * _ClassDkp._Height) + 10;
                panel_DrawDkp.Width = (_CellSizeDraw * _ClassDkp._Width) + 10;
                if (panel_DrawDkp.Height > 550) this.Height = 600 + (panel_DrawDkp.Height - 550);
                if (panel_DrawDkp.Width > 600) this.Width = 800 + (panel_DrawDkp.Width - 600);
                Point _Location = new Point(5, 5);
                for (int nY = 0; nY < _ClassDkp._Height; nY++)
                {
                    for (int nX = 0; nX < _ClassDkp._Width; nX++)
                    {
                        _Dkp[nY][nX].Multiline = true;
                        _Dkp[nY][nX].Size = new Size(_CellSizeDraw, _CellSizeDraw);
                        _Dkp[nY][nX].Location = _Location;
                        _Dkp[nY][nX].Parent = panel_DrawDkp;
                        _Dkp[nY][nX].TextAlign = HorizontalAlignment.Center;
                        _Dkp[nY][nX].BackColor = Color.White;
                        _Dkp[nY][nX].Cursor = Cursors.Cross;
                        _Dkp[nY][nX].ReadOnly = true;
                        _Dkp[nY][nX].Click += new EventHandler(EventClickDkp);
                        _Location.X += _CellSizeDraw;
                    }
                    _Location.X = 5;
                    _Location.Y += _CellSizeDraw;
                }
                //
                if ((_ClassDkp._ElementA.X >= 0) && (_ClassDkp._ElementA.Y >= 0))
                {
                    _Dkp[_ClassDkp._ElementA.Y][_ClassDkp._ElementA.X].Text = "A";
                    _Dkp[_ClassDkp._ElementA.Y][_ClassDkp._ElementA.X].ForeColor = Color.White;
                    _Dkp[_ClassDkp._ElementA.Y][_ClassDkp._ElementA.X].BackColor = Color.DarkGreen;
                }
                if ((_ClassDkp._ElementB.X >= 0) && (_ClassDkp._ElementB.Y >= 0))
                {
                    _Dkp[_ClassDkp._ElementB.Y][_ClassDkp._ElementB.X].Text = "B";
                    _Dkp[_ClassDkp._ElementB.Y][_ClassDkp._ElementB.X].ForeColor = Color.White;
                    _Dkp[_ClassDkp._ElementB.Y][_ClassDkp._ElementB.X].BackColor = Color.DarkBlue;
                }
                if (_ClassDkp._ElementsX.Count > 0)
                {
                    for (int n = 0; n < _ClassDkp._ElementsX.Count; n++)
                    {
                        _Dkp[_ClassDkp._ElementsX[n].Y][_ClassDkp._ElementsX[n].X].Text = "X";
                        _Dkp[_ClassDkp._ElementsX[n].Y][_ClassDkp._ElementsX[n].X].ForeColor = Color.White;
                        _Dkp[_ClassDkp._ElementsX[n].Y][_ClassDkp._ElementsX[n].X].BackColor = Color.DarkRed;
                    }
                }
                for (int nY = 0; nY < _ClassDkp._Height; nY++)
                {
                    _Dkp[nY][0].BackColor = Color.LightGray;
                    _Dkp[nY][_ClassDkp._Width - 1].BackColor = Color.LightGray;
                }
                for (int nX = 0; nX < _ClassDkp._Width; nX++)
                {
                    _Dkp[_ClassDkp._Height - 1][nX].BackColor = Color.LightGray;
                    _Dkp[0][nX].BackColor = Color.LightGray;
                }
            }
        }
        // Перемещение формы
        private void panel_MainTop_MouseDown(object sender, MouseEventArgs e)
        {
            this.Opacity = 0.5;
            _TmpX = Cursor.Position.X;
            _TmpY = Cursor.Position.Y;
            _FMove = true;
        }
        private void panel_MainTop_MouseUp(object sender, MouseEventArgs e)
        {
            this.Opacity = 1.0;
            _FMove = false;
        }
        private void panel_MainTop_MouseMove(object sender, MouseEventArgs e)
        {
            if (_FMove)
            {
                this.Left = this.Left + (Cursor.Position.X - _TmpX);
                this.Top = this.Top + (Cursor.Position.Y - _TmpY);
                _TmpX = Cursor.Position.X;
                _TmpY = Cursor.Position.Y;
            }
        }
        //__________________
        // Обработка кнопок
        private void button_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void button_CreateProject_Click(object sender, EventArgs e)
        {
            if (button_ProjectCreate.ForeColor == Color.White)
            {
                FormCreateProject _FCP = new FormCreateProject(this);
                _FCP.ShowDialog();
            }
            else
            {
                if (MessageBox.Show("Для создания нового файла требуется перезагрузка приложения.\nВыполнить перезагрузку?", "Внимание!") == DialogResult.OK)
                {
                    Application.Restart();
                }
            }

        }
        private void button_ProjectOpen_Click(object sender, EventArgs e)
        {
            if (button_ProjectOpen.ForeColor == Color.White)
            {
                OpenFileDialog _OpenFileDialog = new OpenFileDialog();
                _OpenFileDialog.Filter = "Файлы приложения(*.xml)|*.xml|Все файлы(*.*)|*.*";
                if (_OpenFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        FileStream _FileStream = new FileStream(_OpenFileDialog.FileName, FileMode.Open, FileAccess.Read);
                        XmlDocument _XmlDocument = new XmlDocument();
                        _ClassDkp = new ClassDkp();
                        _XmlDocument.Load(_FileStream);
                        _FileStream.Close();
                        if (_XmlDocument["Root"]["Setting"].Attributes["Name"].Value.Length > 0)
                        {
                            _ClassDkp._NameProject = _XmlDocument["Root"]["Setting"].Attributes["Name"].Value;
                        }
                        else
                        {
                            _ClassDkp._NameProject = Path.GetFileNameWithoutExtension(_OpenFileDialog.FileName);
                        }
                        if (Convert.ToInt32(_ClassDkp._NameProject = _XmlDocument["Root"]["Setting"].Attributes["CellSize"].Value) >= 0)
                        {
                            _ClassDkp._CellSize = Convert.ToInt32(_ClassDkp._NameProject = _XmlDocument["Root"]["Setting"].Attributes["CellSize"].Value);
                        }
                        if ((Convert.ToInt32(_XmlDocument["Root"]["Setting"].Attributes["Heigh"].Value) >= 5) && (Convert.ToInt32(_XmlDocument["Root"]["Setting"].Attributes["Width"].Value) >= 5))
                        {
                            _ClassDkp._Height = Convert.ToInt32(_XmlDocument["Root"]["Setting"].Attributes["Heigh"].Value);
                            _ClassDkp._Width = Convert.ToInt32(_XmlDocument["Root"]["Setting"].Attributes["Width"].Value);
                        }
                        if ((Convert.ToInt32(_XmlDocument["Root"]["ElementA"].Attributes["X"].Value) >= 0) && (Convert.ToInt32(_XmlDocument["Root"]["ElementA"].Attributes["Y"].Value) >= 0))
                        {
                            _ClassDkp._ElementA.X = Convert.ToInt32(Convert.ToInt32(_XmlDocument["Root"]["ElementA"].Attributes["X"].Value));
                            _ClassDkp._ElementA.Y = Convert.ToInt32(Convert.ToInt32(_XmlDocument["Root"]["ElementA"].Attributes["Y"].Value));
                        }
                        if ((Convert.ToInt32(_XmlDocument["Root"]["ElementB"].Attributes["X"].Value) >= 0) && (Convert.ToInt32(_XmlDocument["Root"]["ElementB"].Attributes["Y"].Value) >= 0))
                        {
                            _ClassDkp._ElementB.X = Convert.ToInt32(Convert.ToInt32(_XmlDocument["Root"]["ElementB"].Attributes["X"].Value));
                            _ClassDkp._ElementB.Y = Convert.ToInt32(Convert.ToInt32(_XmlDocument["Root"]["ElementB"].Attributes["Y"].Value));
                        }
                        if (_XmlDocument["Root"]["ElementX"].ChildNodes.Count > 0)
                        {
                            for (int n = 0; n < _XmlDocument["Root"]["ElementX"].ChildNodes.Count; n++)
                            {
                                if (((Convert.ToInt32(_XmlDocument["Root"]["ElementX"].ChildNodes[n].Attributes["X"].Value) >= 0)) && ((Convert.ToInt32(_XmlDocument["Root"]["ElementX"].ChildNodes[n].Attributes["Y"].Value) >= 0)))
                                {
                                    _ClassDkp._ElementsX.Add(new Point(Convert.ToInt32(_XmlDocument["Root"]["ElementX"].ChildNodes[n].Attributes["X"].Value), Convert.ToInt32(Convert.ToInt32(_XmlDocument["Root"]["ElementX"].ChildNodes[n].Attributes["Y"].Value))));
                                }
                            }
                        }
                        button_ProjectCreate.ForeColor = Color.Cyan;
                        button_ProjectOpen.ForeColor = Color.Cyan;
                        button_ProjectSave.Enabled = true;
                        groupBox_Tools.Enabled = true;
                        DrawDKP();
                        if ((_ClassDkp._ElementA.X >= 0) &&(_ClassDkp._ElementA.Y >= 0) &&(_ClassDkp._ElementB.X >= 0) &&(_ClassDkp._ElementB.Y >= 0))
                        {
                            button_WaveCreate.Enabled = true;
                        }
                    }
                    catch (Exception _Exception)
                    {
                        MessageBox.Show("Невозможно загрузить настройки ДКП.", "Ошибка!");
                        MessageBox.Show(_Exception.Message, "Ошибка!");
                    }
                }
            }
            else
            {
                if (MessageBox.Show("Для открытия нового файла требуется перезагрузка приложения.\nВыполнить перезагрузку?", "Внимание!") == DialogResult.OK)
                {
                    Application.Restart();
                }
            }
        }
        private void button_ProjectSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog _SaveFileDialog = new SaveFileDialog();
            _SaveFileDialog.FileName = _ClassDkp._NameProject;
            _SaveFileDialog.Filter = "Файлы приложения(*.xml)|*.xml|Все файлы(*.*)|*.*";
            if (_SaveFileDialog.ShowDialog() == DialogResult.OK)
            {
                XmlDocument _XmlDocument = new XmlDocument();
                _XmlDocument.LoadXml(Properties.Resources.TempateSave);
                if (_ClassDkp._NameProject.Length > 0)
                {
                    _XmlDocument["Root"]["Setting"].Attributes["Name"].Value = _ClassDkp._NameProject;
                }
                if ((_ClassDkp._Height > 0) && (_ClassDkp._Width > 0))
                {
                    _XmlDocument["Root"]["Setting"].Attributes["Heigh"].Value = _ClassDkp._Height.ToString();
                    _XmlDocument["Root"]["Setting"].Attributes["Width"].Value = _ClassDkp._Width.ToString();
                }
                if (_ClassDkp._CellSize >= 0)
                {
                    _ClassDkp._NameProject = _XmlDocument["Root"]["Setting"].Attributes["CellSize"].Value = _ClassDkp._CellSize.ToString();
                }
                if ((_ClassDkp._ElementA.X >= 0) && (_ClassDkp._ElementA.Y >= 0))
                {
                    _XmlDocument["Root"]["ElementA"].Attributes["X"].Value = _ClassDkp._ElementA.X.ToString();
                    _XmlDocument["Root"]["ElementA"].Attributes["Y"].Value = _ClassDkp._ElementA.Y.ToString();
                }
                if ((_ClassDkp._ElementB.X >= 0) && (_ClassDkp._ElementB.Y >= 0))
                {
                    _XmlDocument["Root"]["ElementB"].Attributes["X"].Value = _ClassDkp._ElementB.X.ToString();
                    _XmlDocument["Root"]["ElementB"].Attributes["Y"].Value = _ClassDkp._ElementB.Y.ToString();
                }
                if (_ClassDkp._ElementsX.Count > 0)
                {
                    for (int n = 0; n < _ClassDkp._ElementsX.Count; n++)
                    {
                        XmlNode _XmlNode = _XmlDocument.CreateElement("ElementX");
                        XmlAttribute _XmlAttributeX = _XmlDocument.CreateAttribute("X");
                        _XmlAttributeX.Value = _ClassDkp._ElementsX[n].X.ToString();
                        _XmlNode.Attributes.Append(_XmlAttributeX);
                        XmlAttribute _XmlAttributeY = _XmlDocument.CreateAttribute("Y");
                        _XmlAttributeY.Value = _ClassDkp._ElementsX[n].Y.ToString();
                        _XmlNode.Attributes.Append(_XmlAttributeY);
                        _XmlDocument["Root"]["ElementX"].AppendChild(_XmlNode);
                    }
                }
                FileStream _FileStream = new FileStream(_SaveFileDialog.FileName, FileMode.Create);
                _XmlDocument.Save(_FileStream);
                _FileStream.Close();
            }
        }
        private void button_SetIn_Click(object sender, EventArgs e)
        {
            if (button_SetIn.ForeColor == Color.White)
            {
                button_SetIn.ForeColor = Color.Red;
                button_SetOut.Enabled = false;
                button_SetX.Enabled = false;
                button_WaveCreate.Enabled = false;
                button_Directions.Enabled = false;
                checkBox_DelayUnit.Enabled = false;
            }
            else
            {
                button_SetIn.ForeColor = Color.White;
                button_SetOut.Enabled = true;
                button_SetX.Enabled = true;
                button_Directions.Enabled = true;
                checkBox_DelayUnit.Enabled = true;
                if ((_ClassDkp._ElementA.X >= 0) && (_ClassDkp._ElementA.Y >= 0) && (_ClassDkp._ElementB.X >= 0) && (_ClassDkp._ElementB.Y >= 0))
                {
                    button_WaveCreate.Enabled = true;
                }
            }
        }
        private void button_SetOut_Click(object sender, EventArgs e)
        {
            if (button_SetOut.ForeColor == Color.White)
            {
                button_SetOut.ForeColor = Color.Red;
                button_SetIn.Enabled = false;
                button_SetX.Enabled = false;
                button_WaveCreate.Enabled = false;
                button_Directions.Enabled = false;
                checkBox_DelayUnit.Enabled = false;
            }
            else
            {
                button_SetOut.ForeColor = Color.White;
                button_SetIn.Enabled = true;
                button_SetX.Enabled = true;
                button_Directions.Enabled = true;
                checkBox_DelayUnit.Enabled = true;
                if ((_ClassDkp._ElementA.X >= 0) && (_ClassDkp._ElementA.Y >= 0) && (_ClassDkp._ElementB.X >= 0) && (_ClassDkp._ElementB.Y >= 0))
                {
                    button_WaveCreate.Enabled = true;
                }
            }
        }
        private void button_SetX_Click(object sender, EventArgs e)
        {
            if (button_SetX.ForeColor == Color.White)
            {
                button_SetX.ForeColor = Color.Red;
                button_SetIn.Enabled = false;
                button_SetOut.Enabled = false;
                button_WaveCreate.Enabled = false;
                button_Directions.Enabled = false;
                checkBox_DelayUnit.Enabled = false;
            }
            else
            {
                button_SetX.ForeColor = Color.White;
                button_SetIn.Enabled = true;
                button_SetOut.Enabled = true;
                button_Directions.Enabled = true;
                checkBox_DelayUnit.Enabled = true;
                if ((_ClassDkp._ElementA.X >= 0) && (_ClassDkp._ElementA.Y >= 0) && (_ClassDkp._ElementB.X >= 0) && (_ClassDkp._ElementB.Y >= 0))
                {
                    button_WaveCreate.Enabled = true;
                }
            }
        }
        private void button_WaveCreate_Click(object sender, EventArgs e)
        {
            button_WaveCreate.ForeColor = Color.LightGreen;
            button_SetIn.Enabled = false;
            button_SetOut.Enabled = false;
            button_SetX.Enabled = false;
            button_Directions.Enabled = false;
            checkBox_DelayUnit.Enabled = false;
            button_Tracing.Enabled = true;
            Stack myStack_y0 = new Stack();
            Stack myStack_x0 = new Stack();
            Stack myStack_y1 = new Stack();
            Stack myStack_x1 = new Stack();
            myStack_y0.Push(_ClassDkp._ElementA.Y);
            myStack_x0.Push(_ClassDkp._ElementA.X);
            int num = 0;
            int _nSteps = 0;
            Point _CP = new Point();
            while (_nSteps < (_ClassDkp._Height * _ClassDkp._Width))
            {
                _nSteps++;
                num++;
                while ((myStack_y0.Count != 0) & (myStack_x0.Count != 0))
                {
                    _CP.Y = (int)myStack_y0.Pop();
                    _CP.X = (int)myStack_x0.Pop();
                    if (_CP.X != 0)
                    {
                        if ((_Dkp[_CP.Y][_CP.X - 1].BackColor == Color.White) & (_Dkp[_CP.Y][_CP.X - 1].Text == ""))
                        {
                            _Dkp[_CP.Y][_CP.X - 1].Text = num.ToString();
                            myStack_y1.Push(_CP.Y);
                            myStack_x1.Push(_CP.X - 1);
                        }
                        else
                        {
                            if (checkBox_DelayUnit.Checked == true)
                            {
                                if ((_Dkp[_CP.Y][_CP.X - 1].BackColor == Color.DarkRed) & (_Dkp[_CP.Y][_CP.X - 1].Text == "X"))
                                {
                                    _Dkp[_CP.Y][_CP.X - 1].Text = (num + 2).ToString();
                                    myStack_y1.Push(_CP.Y);
                                    myStack_x1.Push(_CP.X - 1);
                                }
                            }
                        }
                    }
                    if (_CP.Y != _ClassDkp._Height - 1)
                    {
                        if ((_Dkp[_CP.Y + 1][_CP.X].BackColor == Color.White) & (_Dkp[_CP.Y + 1][_CP.X].Text == ""))
                        {
                            _Dkp[_CP.Y + 1][_CP.X].Text = num.ToString();
                            myStack_y1.Push(_CP.Y + 1);
                            myStack_x1.Push(_CP.X);
                        }
                        else
                        {
                            if (checkBox_DelayUnit.Checked == true)
                            {
                                if ((_Dkp[_CP.Y + 1][_CP.X].BackColor == Color.DarkRed) & (_Dkp[_CP.Y + 1][_CP.X].Text == "X"))
                                {
                                    _Dkp[_CP.Y + 1][_CP.X].Text = (num + 2).ToString();
                                    myStack_y1.Push(_CP.Y + 1);
                                    myStack_x1.Push(_CP.X);
                                }
                            }
                        }

                    }
                    if (_CP.X != _ClassDkp._Width - 1)
                    {
                        if ((_Dkp[_CP.Y][_CP.X + 1].BackColor == Color.White) & (_Dkp[_CP.Y][_CP.X + 1].Text == ""))
                        {
                            _Dkp[_CP.Y][_CP.X + 1].Text = num.ToString();
                            myStack_y1.Push(_CP.Y);
                            myStack_x1.Push(_CP.X + 1);
                        }
                        else
                        {
                            if (checkBox_DelayUnit.Checked == true)
                            {
                                if ((_Dkp[_CP.Y][_CP.X + 1].BackColor == Color.DarkRed) & (_Dkp[_CP.Y][_CP.X + 1].Text == "X"))
                                {
                                    _Dkp[_CP.Y][_CP.X + 1].Text = (num + 2).ToString();
                                    myStack_y1.Push(_CP.Y);
                                    myStack_x1.Push(_CP.X + 1);
                                }
                            }
                        }
                    }
                    if (_CP.Y != 0)
                    {
                        if ((_Dkp[_CP.Y - 1][_CP.X].BackColor == Color.White) & (_Dkp[_CP.Y - 1][_CP.X].Text == ""))
                        {
                            _Dkp[_CP.Y - 1][_CP.X].Text = num.ToString();
                            myStack_y1.Push(_CP.Y - 1);
                            myStack_x1.Push(_CP.X);
                        }
                        else
                        {
                            if (checkBox_DelayUnit.Checked == true)
                            {
                                if ((_Dkp[_CP.Y - 1][_CP.X].BackColor == Color.DarkRed) & (_Dkp[_CP.Y - 1][_CP.X].Text == "X"))
                                {
                                    _Dkp[_CP.Y - 1][_CP.X].Text = (num + 2).ToString();
                                    myStack_y1.Push(_CP.Y - 1);
                                    myStack_x1.Push(_CP.X);
                                }
                            }
                        }
                    }
                }
                while ((myStack_y1.Count != 0) & (myStack_x1.Count != 0))
                {
                    myStack_y0.Push(myStack_y1.Pop());
                    myStack_x0.Push(myStack_x1.Pop());
                }
            }
        }
        private void button_Directions_Click(object sender, EventArgs e)
        {
            FormSetDirections _FSD = new FormSetDirections(this);
            _FSD.ShowDialog();
        }
        private void button_Tracing_Click(object sender, EventArgs e)
        {
            button_Tracing.Enabled = false;
            button_SetIn.Enabled = false;
            button_SetOut.Enabled = false;
            button_SetX.Enabled = false;
            button_WaveCreate.Enabled = false;
            button_Directions.Enabled = false;
            checkBox_DelayUnit.Enabled = false;
            int S = 0;
            if (checkBox_DelayUnit.Checked)
            {
                S = RealiseTracing_Mod();
            }
            else
            {
                S = RealiseTracing_noMod();
            }
            MessageBox.Show("Проложен путь, длиной в " + (S * _ClassDkp._CellSize).ToString() + " мм","Результат:");
        }
        //__________________
        private void EventClickDkp(object sender, System.EventArgs e)
        {
            if (button_SetIn.ForeColor == Color.Red)
            {
                if ((_ClassDkp._ElementA.X >= 0) && (_ClassDkp._ElementA.Y >= 0))
                {
                    _Dkp[_ClassDkp._ElementA.Y][_ClassDkp._ElementA.X].Text = "";
                    _Dkp[_ClassDkp._ElementA.Y][_ClassDkp._ElementA.X].ForeColor = Color.Black;
                    _Dkp[_ClassDkp._ElementA.Y][_ClassDkp._ElementA.X].BackColor = Color.White;
                    _ClassDkp._ElementA.X = -1;
                    _ClassDkp._ElementA.Y = -1;
                }
                TextBox _TextBox = (TextBox)sender;
                if (_TextBox.BackColor == Color.White)
                {
                    _TextBox.Text = "A";
                    _TextBox.ForeColor = Color.White;
                    _TextBox.BackColor = Color.DarkGreen;
                    sender = _TextBox;
                    for (int nY = 0; nY < _ClassDkp._Height; nY++)
                    {
                        for (int nX = 0; nX < _ClassDkp._Width; nX++)
                        {
                            if (_Dkp[nY][nX].Text == "A")
                            {
                                _ClassDkp._ElementA.X = nX;
                                _ClassDkp._ElementA.Y = nY;
                            }
                        }
                    }
                }
            }
            if (button_SetOut.ForeColor == Color.Red)
            {
                if ((_ClassDkp._ElementB.X >= 0) && (_ClassDkp._ElementB.Y >= 0))
                {
                    _Dkp[_ClassDkp._ElementB.Y][_ClassDkp._ElementB.X].Text = "";
                    _Dkp[_ClassDkp._ElementB.Y][_ClassDkp._ElementB.X].ForeColor = Color.Black;
                    _Dkp[_ClassDkp._ElementB.Y][_ClassDkp._ElementB.X].BackColor = Color.White;
                    _ClassDkp._ElementB.X = -1;
                    _ClassDkp._ElementB.Y = -1;
                }
                TextBox _TextBox = (TextBox)sender;
                if (_TextBox.BackColor == Color.White)
                {
                    _TextBox.Text = "B";
                    _TextBox.ForeColor = Color.White;
                    _TextBox.BackColor = Color.DarkBlue;
                    sender = _TextBox;
                    for (int nY = 0; nY < _ClassDkp._Height; nY++)
                    {
                        for (int nX = 0; nX < _ClassDkp._Width; nX++)
                        {
                            if (_Dkp[nY][nX].Text == "B")
                            {
                                _ClassDkp._ElementB.X = nX;
                                _ClassDkp._ElementB.Y = nY;
                            }
                        }
                    }
                }
            }
            if (button_SetX.ForeColor == Color.Red)
            {
                for (int nY = 0; nY < _ClassDkp._Height; nY++)
                {
                    for (int nX = 0; nX < _ClassDkp._Width; nX++)
                    {
                        if (_Dkp[nY][nX].Focused == true)
                        {
                            bool _FSet = true;
                            if (_ClassDkp._ElementsX.Count > 0)
                            {
                                for (int n = 0; n < _ClassDkp._ElementsX.Count; n++)
                                {
                                    if ((nY == _ClassDkp._ElementsX[n].Y) && (nX == _ClassDkp._ElementsX[n].X))
                                    {
                                        _Dkp[nY][nX].Text = "";
                                        _Dkp[nY][nX].ForeColor = Color.Black;
                                        _Dkp[nY][nX].BackColor = Color.White;
                                        _FSet = false;
                                        _ClassDkp._ElementsX.RemoveAt(n);
                                    }
                                }
                            }
                            if (_FSet)
                            {
                                _Dkp[nY][nX].Text = "X";
                                _Dkp[nY][nX].ForeColor = Color.White;
                                _Dkp[nY][nX].BackColor = Color.DarkRed;
                                _ClassDkp._ElementsX.Add(new Point(nX, nY));
                            }
                        }
                    }
                }
            }
        }
        // Рабоачие функции
        private int RealiseTracing_noMod()
        {
            int _ValueTop, _ValueTopRight, _ValueRight, _ValueBottonRight, _ValueBotton, _ValueBottonLeft, _ValueLeft, _ValueTopLeft;
            int _Ay = 0, _Ax = 0, _By = 0, _Bx = 0, _Cy = 0, _Cx = 0;
            bool fff = false;
            _Ay = _ClassDkp._ElementA.Y;
            _Ax = _ClassDkp._ElementA.X;
            _By = _ClassDkp._ElementB.Y;
            _Bx = _ClassDkp._ElementB.X;
            int S = 0;
            while (true)
            {
                for (int n = 1; n <= 8; n++)
                {
                    _ValueTop = 0;
                    _ValueTopRight = 0;
                    _ValueRight = 0;
                    _ValueBottonRight = 0;
                    _ValueBotton = 0;
                    _ValueBottonLeft = 0;
                    _ValueLeft = 0;
                    _ValueTopLeft = 0;
                    // Проверка <Top>
                    if (n == _ClassDkp._Directions[0])
                    {
                        if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                        {
                            _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                            // Проверка <Top>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueTop > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Top>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueTop > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Top>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueTop > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Top>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueTop > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Top>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueTop > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Top>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueTop > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Top>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueTop > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By - 1;
                                _Cx = _Bx;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <TopRight>
                    if (n == _ClassDkp._Directions[1])
                    {
                        if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                        {
                            _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                            // Проверка <TopRight>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueTopRight > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopRight>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueTopRight > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopRight>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueTopRight > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopRight>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueTopRight > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopRight>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueTopRight > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopRight>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueTopRight > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopRight>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueTopRight > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By - 1;
                                _Cx = _Bx + 1;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <Right>
                    if (n == _ClassDkp._Directions[2])
                    {
                        if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                        {
                            _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                            // Проверка <Right>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueRight > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Right>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueRight > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Right>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueRight > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Right>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueRight > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Right>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueRight > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Right>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueRight > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Right>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueRight > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By;
                                _Cx = _Bx + 1;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <BottonRight>
                    if (n == _ClassDkp._Directions[3])
                    {
                        if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                        {
                            _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                            // Проверка <BottonRight>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueBottonRight > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonRight>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueBottonRight > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonRight>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueBottonRight > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonRight>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueBottonRight > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonRight>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueBottonRight > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonRight>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueBottonRight > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonRight>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueBottonRight > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By + 1;
                                _Cx = _Bx + 1;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <Botton>
                    if (n == _ClassDkp._Directions[4])
                    {
                        if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                        {
                            _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                            // Проверка <Botton>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueBotton > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Botton>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueBotton > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Botton>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueBotton > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Botton>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueBotton > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Botton>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueBotton > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Botton>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueBotton > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Botton>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueBotton > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By + 1;
                                _Cx = _Bx;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <BottonLeft>
                    if (n == _ClassDkp._Directions[5])
                    {
                        if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                        {
                            _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                            // Проверка <BottonLeft>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueBottonLeft > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonLeft>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueBottonLeft > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonLeft>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueBottonLeft > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonLeft>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueBottonLeft > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonLeft>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueBottonLeft > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonLeft>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueBottonLeft > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonLeft>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueBottonLeft > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By + 1;
                                _Cx = _Bx - 1;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <Left>
                    if (n == _ClassDkp._Directions[6])
                    {
                        if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                        {
                            _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                            // Проверка <Left>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueLeft > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Left>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueLeft > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Left>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueLeft > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Left>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueLeft > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Left>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueLeft > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Left>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueLeft > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Left>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueLeft > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By;
                                _Cx = _Bx - 1;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <TopLeft>
                    if (n == _ClassDkp._Directions[7])
                    {
                        if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                        {
                            _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                            // Проверка <TopLeft>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueTopLeft > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopLeft>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueTopLeft > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopLeft>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueTopLeft > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopLeft>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueTopLeft > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopLeft>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueTopLeft > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopLeft>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueTopLeft > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopLeft>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueTopLeft > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By - 1;
                                _Cx = _Bx - 1;
                            }
                            fff = false;
                        }
                    }
                }
                _Dkp[_Cy][_Cx].BackColor = Color.LawnGreen;
                S++;
                _By = _Cy;
                _Bx = _Cx;
                if ((_Cy - 1 == _Ay) & (_Cx == _Ax)) break;
                if ((_Cy - 1 == _Ay) & (_Cx + 1 == _Ax)) break;
                if ((_Cy == _Ay) & (_Cx + 1 == _Ax)) break;
                if ((_Cy + 1 == _Ay) & (_Cx + 1 == _Ax)) break;
                if ((_Cy + 1 == _Ay) & (_Cx == _Ax)) break;
                if ((_Cy + 1 == _Ay) & (_Cx - 1 == _Ax)) break;
                if ((_Cy == _Ay) & (_Cx - 1 == _Ax)) break;
                if ((_Cy - 1 == _Ay) & (_Cx - 1 == _Ax)) break;
            }
            return S;
        }
        private int RealiseTracing_Mod()
        {
            int _ValueTop, _ValueTopRight, _ValueRight, _ValueBottonRight, _ValueBotton, _ValueBottonLeft, _ValueLeft, _ValueTopLeft;
            int _Ay = 0, _Ax = 0, _By = 0, _Bx = 0, _Cy = 0, _Cx = 0;
            bool fff = false;
            _Ay = _ClassDkp._ElementA.Y;
            _Ax = _ClassDkp._ElementA.X;
            _By = _ClassDkp._ElementB.Y;
            _Bx = _ClassDkp._ElementB.X;

            for (int i=0; i < _ClassDkp._ElementsX.Count; i++)
            {
                _Dkp[_ClassDkp._ElementsX[i].Y][_ClassDkp._ElementsX[i].X].BackColor = Color.White;
            }
            int S = 0;
            while (true)
            {
                for (int n = 1; n <= 8; n++)
                {
                    _ValueTop = 0;
                    _ValueTopRight = 0;
                    _ValueRight = 0;
                    _ValueBottonRight = 0;
                    _ValueBotton = 0;
                    _ValueBottonLeft = 0;
                    _ValueLeft = 0;
                    _ValueTopLeft = 0;
                    // Проверка <Top>
                    if (n == _ClassDkp._Directions[0])
                    {
                        if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                        {
                            _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                            // Проверка <Top>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueTop > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Top>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueTop > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Top>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueTop > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Top>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueTop > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Top>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueTop > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Top>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueTop > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Top>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueTop > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By - 1;
                                _Cx = _Bx;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <TopRight>
                    if (n == _ClassDkp._Directions[1])
                    {
                        if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                        {
                            _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                            // Проверка <TopRight>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueTopRight > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopRight>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueTopRight > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopRight>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueTopRight > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopRight>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueTopRight > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopRight>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueTopRight > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopRight>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueTopRight > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopRight>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueTopRight > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By - 1;
                                _Cx = _Bx + 1;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <Right>
                    if (n == _ClassDkp._Directions[2])
                    {
                        if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                        {
                            _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                            // Проверка <Right>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueRight > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Right>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueRight > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Right>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueRight > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Right>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueRight > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Right>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueRight > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Right>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueRight > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Right>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueRight > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By;
                                _Cx = _Bx + 1;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <BottonRight>
                    if (n == _ClassDkp._Directions[3])
                    {
                        if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                        {
                            _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                            // Проверка <BottonRight>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueBottonRight > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonRight>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueBottonRight > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonRight>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueBottonRight > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonRight>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueBottonRight > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonRight>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueBottonRight > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonRight>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueBottonRight > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonRight>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueBottonRight > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By + 1;
                                _Cx = _Bx + 1;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <Botton>
                    if (n == _ClassDkp._Directions[4])
                    {
                        if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                        {
                            _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                            // Проверка <Botton>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueBotton > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Botton>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueBotton > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Botton>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueBotton > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Botton>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueBotton > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Botton>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueBotton > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Botton>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueBotton > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Botton>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueBotton > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By + 1;
                                _Cx = _Bx;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <BottonLeft>
                    if (n == _ClassDkp._Directions[5])
                    {
                        if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                        {
                            _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                            // Проверка <BottonLeft>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueBottonLeft > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonLeft>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueBottonLeft > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonLeft>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueBottonLeft > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonLeft>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueBottonLeft > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonLeft>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueBottonLeft > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonLeft>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueBottonLeft > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <BottonLeft>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueBottonLeft > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By + 1;
                                _Cx = _Bx - 1;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <Left>
                    if (n == _ClassDkp._Directions[6])
                    {
                        if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                        {
                            _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                            // Проверка <Left>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueLeft > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Left>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueLeft > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Left>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueLeft > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Left>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueLeft > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Left>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueLeft > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Left>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueLeft > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <Left>: относительно <TopLeft>
                            if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                                if (_ValueLeft > _ValueTopLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By;
                                _Cx = _Bx - 1;
                            }
                            fff = false;
                        }
                    }
                    // Проверка <TopLeft>
                    if (n == _ClassDkp._Directions[7])
                    {
                        if (_Dkp[_By - 1][_Bx - 1].BackColor == Color.White)
                        {
                            _ValueTopLeft = Convert.ToInt32(_Dkp[_By - 1][_Bx - 1].Text);
                            // Проверка <TopLeft>: относительно <Top>
                            if (_Dkp[_By - 1][_Bx].BackColor == Color.White)
                            {
                                _ValueTop = Convert.ToInt32(_Dkp[_By - 1][_Bx].Text);
                                if (_ValueTopLeft > _ValueTop)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopLeft>: относительно <TopRight>
                            if (_Dkp[_By - 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueTopRight = Convert.ToInt32(_Dkp[_By - 1][_Bx + 1].Text);
                                if (_ValueTopLeft > _ValueTopRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopLeft>: относительно <Right>
                            if (_Dkp[_By][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueRight = Convert.ToInt32(_Dkp[_By][_Bx + 1].Text);
                                if (_ValueTopLeft > _ValueRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopLeft>: относительно <BottonRight>
                            if (_Dkp[_By + 1][_Bx + 1].BackColor == Color.White)
                            {
                                _ValueBottonRight = Convert.ToInt32(_Dkp[_By + 1][_Bx + 1].Text);
                                if (_ValueTopLeft > _ValueBottonRight)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopLeft>: относительно <Botton>
                            if (_Dkp[_By + 1][_Bx].BackColor == Color.White)
                            {
                                _ValueBotton = Convert.ToInt32(_Dkp[_By + 1][_Bx].Text);
                                if (_ValueTopLeft > _ValueBotton)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopLeft>: относительно <BottonLeft>
                            if (_Dkp[_By + 1][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueBottonLeft = Convert.ToInt32(_Dkp[_By + 1][_Bx - 1].Text);
                                if (_ValueTopLeft > _ValueBottonLeft)
                                {
                                    fff = true;
                                }
                            }
                            // Проверка <TopLeft>: относительно <Left>
                            if (_Dkp[_By][_Bx - 1].BackColor == Color.White)
                            {
                                _ValueLeft = Convert.ToInt32(_Dkp[_By][_Bx - 1].Text);
                                if (_ValueTopLeft > _ValueLeft)
                                {
                                    fff = true;
                                }
                            }
                            if (fff == false)
                            {
                                _Cy = _By - 1;
                                _Cx = _Bx - 1;
                            }
                            fff = false;
                        }
                    }
                }
                _Dkp[_Cy][_Cx].BackColor = Color.LawnGreen;
                S++;
                _By = _Cy;
                _Bx = _Cx;
                if ((_Cy - 1 == _Ay) & (_Cx == _Ax)) break;
                if ((_Cy - 1 == _Ay) & (_Cx + 1 == _Ax)) break;
                if ((_Cy == _Ay) & (_Cx + 1 == _Ax)) break;
                if ((_Cy + 1 == _Ay) & (_Cx + 1 == _Ax)) break;
                if ((_Cy + 1 == _Ay) & (_Cx == _Ax)) break;
                if ((_Cy + 1 == _Ay) & (_Cx - 1 == _Ax)) break;
                if ((_Cy == _Ay) & (_Cx - 1 == _Ax)) break;
                if ((_Cy - 1 == _Ay) & (_Cx - 1 == _Ax)) break;
            }
            for (int i = 0; i < _ClassDkp._ElementsX.Count; i++)
            {
                if (_Dkp[_ClassDkp._ElementsX[i].Y][_ClassDkp._ElementsX[i].X].BackColor == Color.LawnGreen)
                {
                    _Dkp[_ClassDkp._ElementsX[i].Y][_ClassDkp._ElementsX[i].X].ForeColor = Color.DarkRed;
                }
                else
                {
                    _Dkp[_ClassDkp._ElementsX[i].Y][_ClassDkp._ElementsX[i].X].BackColor = Color.DarkRed;
                }
            }
            return S;
        }

        private void button_Help_Click(object sender, EventArgs e)
        {
            FormHelp _FH = new FormHelp(1);
            _FH.ShowDialog();
        }
    }
}
