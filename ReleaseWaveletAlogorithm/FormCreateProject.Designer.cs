﻿namespace ReleaseWaveletAlogorithm
{
    partial class FormCreateProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_MainTop = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_Main = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_NameProject = new System.Windows.Forms.TextBox();
            this.textBox_DkpCell = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox_DkpHeight = new System.Windows.Forms.ComboBox();
            this.comboBox_DkpWidth = new System.Windows.Forms.ComboBox();
            this.panel_MainBotton = new System.Windows.Forms.Panel();
            this.button_Help = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.button_Create = new System.Windows.Forms.Button();
            this.panel_MainTop.SuspendLayout();
            this.panel_Main.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel_MainBotton.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_MainTop
            // 
            this.panel_MainTop.BackColor = System.Drawing.Color.White;
            this.panel_MainTop.Controls.Add(this.label1);
            this.panel_MainTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_MainTop.Font = new System.Drawing.Font("Open Sans", 15.75F, System.Drawing.FontStyle.Bold);
            this.panel_MainTop.Location = new System.Drawing.Point(0, 0);
            this.panel_MainTop.Name = "panel_MainTop";
            this.panel_MainTop.Size = new System.Drawing.Size(648, 33);
            this.panel_MainTop.TabIndex = 0;
            this.panel_MainTop.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_MainTop_MouseDown);
            this.panel_MainTop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_MainTop_MouseMove);
            this.panel_MainTop.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_MainTop_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.label1.Location = new System.Drawing.Point(3, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Новый проект";
            // 
            // panel_Main
            // 
            this.panel_Main.BackColor = System.Drawing.Color.Silver;
            this.panel_Main.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Main.Controls.Add(this.tableLayoutPanel1);
            this.panel_Main.Controls.Add(this.panel_MainBotton);
            this.panel_Main.Controls.Add(this.panel_MainTop);
            this.panel_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Main.Location = new System.Drawing.Point(0, 0);
            this.panel_Main.Name = "panel_Main";
            this.panel_Main.Size = new System.Drawing.Size(650, 300);
            this.panel_Main.TabIndex = 1;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.85811F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.14189F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox_NameProject, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox_DkpCell, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label9, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_DkpHeight, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_DkpWidth, 1, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Lay\'s", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 33);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(648, 210);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(86, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(215, 25);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ширина поля ДКП:";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(81, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(220, 25);
            this.label5.TabIndex = 0;
            this.label5.Text = "Название проекта:";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(291, 25);
            this.label4.TabIndex = 0;
            this.label4.Text = "Размерность ячейки ДКП:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(92, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Высота поля ДКП:";
            // 
            // textBox_NameProject
            // 
            this.textBox_NameProject.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_NameProject.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_NameProject.Location = new System.Drawing.Point(307, 23);
            this.textBox_NameProject.Name = "textBox_NameProject";
            this.textBox_NameProject.Size = new System.Drawing.Size(274, 33);
            this.textBox_NameProject.TabIndex = 1;
            // 
            // textBox_DkpCell
            // 
            this.textBox_DkpCell.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_DkpCell.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_DkpCell.Location = new System.Drawing.Point(307, 128);
            this.textBox_DkpCell.Name = "textBox_DkpCell";
            this.textBox_DkpCell.Size = new System.Drawing.Size(120, 33);
            this.textBox_DkpCell.TabIndex = 4;
            this.textBox_DkpCell.Text = "10";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(589, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 18);
            this.label6.TabIndex = 5;
            this.label6.Text = "(.xml)";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(589, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 18);
            this.label7.TabIndex = 5;
            this.label7.Text = "(ед.)";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(589, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 18);
            this.label8.TabIndex = 5;
            this.label8.Text = "(ед.)";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Open Sans", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(589, 133);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 18);
            this.label9.TabIndex = 5;
            this.label9.Text = "(мм)";
            // 
            // comboBox_DkpHeight
            // 
            this.comboBox_DkpHeight.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox_DkpHeight.FormattingEnabled = true;
            this.comboBox_DkpHeight.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30"});
            this.comboBox_DkpHeight.Location = new System.Drawing.Point(307, 58);
            this.comboBox_DkpHeight.Name = "comboBox_DkpHeight";
            this.comboBox_DkpHeight.Size = new System.Drawing.Size(121, 33);
            this.comboBox_DkpHeight.TabIndex = 6;
            this.comboBox_DkpHeight.Text = "15";
            // 
            // comboBox_DkpWidth
            // 
            this.comboBox_DkpWidth.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox_DkpWidth.FormattingEnabled = true;
            this.comboBox_DkpWidth.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30"});
            this.comboBox_DkpWidth.Location = new System.Drawing.Point(307, 93);
            this.comboBox_DkpWidth.Name = "comboBox_DkpWidth";
            this.comboBox_DkpWidth.Size = new System.Drawing.Size(121, 33);
            this.comboBox_DkpWidth.TabIndex = 7;
            this.comboBox_DkpWidth.Text = "15";
            // 
            // panel_MainBotton
            // 
            this.panel_MainBotton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.panel_MainBotton.Controls.Add(this.button_Help);
            this.panel_MainBotton.Controls.Add(this.button_Cancel);
            this.panel_MainBotton.Controls.Add(this.button_Create);
            this.panel_MainBotton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_MainBotton.Font = new System.Drawing.Font("Lay\'s", 15.75F, System.Drawing.FontStyle.Bold);
            this.panel_MainBotton.ForeColor = System.Drawing.Color.White;
            this.panel_MainBotton.Location = new System.Drawing.Point(0, 243);
            this.panel_MainBotton.Name = "panel_MainBotton";
            this.panel_MainBotton.Size = new System.Drawing.Size(648, 55);
            this.panel_MainBotton.TabIndex = 1;
            // 
            // button_Help
            // 
            this.button_Help.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Help.FlatAppearance.BorderSize = 0;
            this.button_Help.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Help.ForeColor = System.Drawing.Color.Transparent;
            this.button_Help.Location = new System.Drawing.Point(181, 6);
            this.button_Help.Name = "button_Help";
            this.button_Help.Size = new System.Drawing.Size(59, 46);
            this.button_Help.TabIndex = 1;
            this.button_Help.Text = "?";
            this.button_Help.UseVisualStyleBackColor = true;
            this.button_Help.Click += new System.EventHandler(this.button_Help_Click);
            // 
            // button_Cancel
            // 
            this.button_Cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.button_Cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Cancel.Dock = System.Windows.Forms.DockStyle.Left;
            this.button_Cancel.FlatAppearance.BorderSize = 0;
            this.button_Cancel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Cancel.Location = new System.Drawing.Point(0, 0);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(175, 55);
            this.button_Cancel.TabIndex = 0;
            this.button_Cancel.Text = "Отмена";
            this.button_Cancel.UseVisualStyleBackColor = false;
            this.button_Cancel.Click += new System.EventHandler(this.button_Cancel_Click);
            // 
            // button_Create
            // 
            this.button_Create.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(36)))), ((int)(((byte)(43)))));
            this.button_Create.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_Create.Dock = System.Windows.Forms.DockStyle.Right;
            this.button_Create.FlatAppearance.BorderSize = 0;
            this.button_Create.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(65)))), ((int)(((byte)(77)))));
            this.button_Create.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Create.Location = new System.Drawing.Point(473, 0);
            this.button_Create.Name = "button_Create";
            this.button_Create.Size = new System.Drawing.Size(175, 55);
            this.button_Create.TabIndex = 0;
            this.button_Create.Text = "Создать";
            this.button_Create.UseVisualStyleBackColor = false;
            this.button_Create.Click += new System.EventHandler(this.button_Create_Click);
            // 
            // FormCreateProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 300);
            this.Controls.Add(this.panel_Main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormCreateProject";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Новый проект";
            this.panel_MainTop.ResumeLayout(false);
            this.panel_MainTop.PerformLayout();
            this.panel_Main.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel_MainBotton.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_MainTop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel_Main;
        private System.Windows.Forms.Panel panel_MainBotton;
        private System.Windows.Forms.Button button_Create;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox_NameProject;
        private System.Windows.Forms.TextBox textBox_DkpCell;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox_DkpHeight;
        private System.Windows.Forms.ComboBox comboBox_DkpWidth;
        private System.Windows.Forms.Button button_Help;
    }
}